using Ecs.Generated.Installer;
using Playdarium.EcsInstallerGenerator.Runtime;
using Playdarium.EcsInstallerGenerator.Runtime.Attributes;
using Playdarium.EcsInstallerGenerator.Runtime.Enums;

namespace Ecs.Scripts
{
	[DebugSystem]
	[Install(ExecutionType.Other, ExecutionPriority.Normal, 1000)]
	public class OtherSystem : IDefiniteBindRuled
	{
	}
}