using Ecs.Generated.Installer;
using Playdarium.EcsInstallerGenerator.Runtime.Attributes;
using Playdarium.EcsInstallerGenerator.Runtime.Enums;

namespace Ecs.Scripts
{
	[Install(ExecutionType.Game, ExecutionPriority.Normal, 1000, "Initialize")]
	public class GameSystem
	{
	}
}