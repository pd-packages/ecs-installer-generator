using Ecs.Generated.Installer;
using Playdarium.EcsInstallerGenerator.Runtime.Attributes;
using Playdarium.EcsInstallerGenerator.Runtime.Enums;

namespace Ecs.Scripts
{
	[Install(ExecutionType.Project, ExecutionPriority.Normal, 1000)]
	public class ProjectSystem
	{
	}
}