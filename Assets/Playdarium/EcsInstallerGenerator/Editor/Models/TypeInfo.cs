using System;
using Playdarium.EcsInstallerGenerator.Runtime.Attributes;

namespace Playdarium.EcsInstallerGenerator.Models
{
	public class TypeInfo
	{
		public readonly Type Type;
		public readonly int Order;
		public readonly string Features;

		public TypeInfo(Type type, InstallAttribute attribute)
		{
			Type = type;
			Order = attribute.Order;
			Features = string.Join(", ", attribute.Features);
		}

		public bool HasAttribute<T>()
			where T : Attribute
			=> Type.IsDefined(typeof(T), false);
	}
}