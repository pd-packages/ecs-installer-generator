﻿using System;
using Playdarium.EcsInstallerGenerator.Runtime.Enums;

namespace Playdarium.EcsInstallerGenerator.Models
{
	[Serializable]
	internal class AttributeChanges
	{
		public long Type;
		public ExecutionPriority Priority;
		public int Order;
		public string Name;
	}
}