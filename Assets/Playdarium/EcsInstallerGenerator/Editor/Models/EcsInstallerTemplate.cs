using System;
using System.Collections.Generic;
using Playdarium.EcsInstallerGenerator.Runtime.Enums;

namespace Playdarium.EcsInstallerGenerator.Models
{
	internal class EcsInstallerTemplate
	{
		public readonly string Type;
		public readonly long TypeFlag;
		public readonly Dictionary<ExecutionPriority, List<TypeInfo>> Container;
		public readonly HashSet<string> Namespaces = new();

		public string Name => $"{Type}EcsSystems";

		public string GeneratedInstallerCode;
		public int Counter;


		public EcsInstallerTemplate(string type, long flag)
		{
			Type = type;
			TypeFlag = flag;
			Container = GetContainer();
		}

		private static Dictionary<ExecutionPriority, List<TypeInfo>> GetContainer()
		{
			var priorities = Enum.GetValues(typeof(ExecutionPriority)) as ExecutionPriority[];
			var dictionary = new Dictionary<ExecutionPriority, List<TypeInfo>>();
			for (var i = 0; i < priorities.Length; i++)
			{
				var priority = priorities[i];
				dictionary.Add(priority, new List<TypeInfo>());
			}

			return dictionary;
		}

		protected bool Equals(EcsInstallerTemplate other) => Type == other.Type;

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != GetType()) return false;
			return Equals((EcsInstallerTemplate)obj);
		}

		public override int GetHashCode() => Type.GetHashCode();
	}
}