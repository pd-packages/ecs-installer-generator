namespace Playdarium.EcsInstallerGenerator.Models
{
	internal class GeneratedFile
	{
		public readonly string Name;
		public readonly string GeneratedInstallerCode;

		public GeneratedFile(string name, string generatedInstallerCode)
		{
			Name = name;
			GeneratedInstallerCode = generatedInstallerCode;
		}
	}
}