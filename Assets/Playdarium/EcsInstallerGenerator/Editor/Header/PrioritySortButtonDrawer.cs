using Playdarium.EcsInstallerGenerator.Sort.Impls;
using UnityEngine;

namespace Playdarium.EcsInstallerGenerator.Header
{
	internal class PrioritySortButtonDrawer : ASortButtonDrawer
	{
		private readonly PrioritySortStrategy _sortStrategy;

		public PrioritySortButtonDrawer(PrioritySortStrategy sortStrategy, GUILayoutOption[] style)
			: base(sortStrategy, style)
		{
			_sortStrategy = sortStrategy;
		}

		protected override string GetButtonLabel() => string.IsNullOrEmpty(_sortStrategy.Name)
			? "Priority"
			: _sortStrategy.Name;
	}
}