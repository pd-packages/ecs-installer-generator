using Playdarium.EcsInstallerGenerator.Sort.Impls;
using UnityEngine;

namespace Playdarium.EcsInstallerGenerator.Header
{
	internal class NameSortButtonDrawer : ASortButtonDrawer
	{
		private readonly NameSortStrategy _sortStrategy;

		public NameSortButtonDrawer(NameSortStrategy sortStrategy, GUILayoutOption[] style)
			: base(sortStrategy, style)
		{
			_sortStrategy = sortStrategy;
		}

		protected override string GetButtonLabel() => "Name " + _sortStrategy.Name;
	}
}