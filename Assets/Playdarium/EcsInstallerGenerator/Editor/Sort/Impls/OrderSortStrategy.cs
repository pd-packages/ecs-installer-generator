using Playdarium.EcsInstallerGenerator.Models;

namespace Playdarium.EcsInstallerGenerator.Sort.Impls
{
	internal class OrderSortStrategy : AAscendingOrDescendingSortStrategy<int>
	{
		protected override int GetValue(AttributeRecord record) => record.Attribute.Order;
	}
}