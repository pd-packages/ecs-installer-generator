using System.Collections.Generic;
using Playdarium.EcsInstallerGenerator.Models;

namespace Playdarium.EcsInstallerGenerator.Sort
{
	internal interface ISortStrategy
	{
		string Name { get; }

		void Next();

		void Reset();

		void Sort(List<AttributeRecord> records);
	}
}