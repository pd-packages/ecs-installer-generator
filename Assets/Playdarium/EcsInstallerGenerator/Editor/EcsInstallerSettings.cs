using System;
using Playdarium.ProjectSettings;

namespace Playdarium.EcsInstallerGenerator
{
	internal class EcsInstallerSettings
	{
		private static readonly ProjectSettingsEditor<SettingsVo> Settings = new("EcsInstallerSettings");

		public static string SavePath
		{
			get => Settings.Read().savePath;
			set => Settings.Change(settings => settings.savePath = value);
		}

		public static string SearchPath
		{
			get => Settings.Read().searchPath;
			set => Settings.Change(settings => settings.searchPath = value);
		}

		public static string[] ExecutionTypes
		{
			get => Settings.Read().executionTypes;
			set => Settings.Change(s => s.executionTypes = value);
		}

		[Serializable]
		private class SettingsVo
		{
			public string savePath = "Ecs/Installers/";
			public string searchPath = "Ecs/";
			public string[] executionTypes = { "Project", "Game" };
		}
	}
}