using System.Collections.Generic;

namespace Playdarium.EcsInstallerGenerator.Utils
{
	internal static class EnumUtils
	{
		public static bool HasFlag(long flagsA, long flagsB)
		{
			return (flagsA & flagsB) > 0;
		}

		public static string[] ExecutionTypeMaskToValuesArray(long mask)
		{
			var list = new List<string>();

			var executionTypes = EcsInstallerSettings.ExecutionTypes;
			for (var i = 0; i < executionTypes.Length; i++)
			{
				if (HasFlag(mask, 1 << i))
					list.Add(executionTypes[i]);
			}

			return list.ToArray();
		}
	}
}