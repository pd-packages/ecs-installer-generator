using System.Collections.Generic;

namespace Playdarium.EcsInstallerGenerator.Utils
{
	internal static class EnumerableExtensions
	{
		public static string Join(this IEnumerable<string> enumerable, string separator)
			=> string.Join(separator, enumerable);
	}
}