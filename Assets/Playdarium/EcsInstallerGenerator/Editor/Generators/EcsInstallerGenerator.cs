﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Playdarium.EcsInstallerGenerator.Models;
using Playdarium.EcsInstallerGenerator.Runtime.Attributes;
using Playdarium.EcsInstallerGenerator.Runtime.Enums;
using Playdarium.EcsInstallerGenerator.Utils;
using UnityEngine;
using TypeInfo = Playdarium.EcsInstallerGenerator.Models.TypeInfo;

namespace Playdarium.EcsInstallerGenerator.Generators
{
	internal class EcsInstallerGenerator
	{
		private static readonly HashSet<EcsInstallerTemplate> EcsInstallers = new();

		public static GeneratedFile[] Generate()
		{
			var executionTypes = EcsInstallerSettings.ExecutionTypes;
			for (var i = 0; i < executionTypes.Length; i++)
			{
				var executionType = executionTypes[i];
				EcsInstallers.Add(new EcsInstallerTemplate(executionType, 1 << i));
			}

			var assemblies = AppDomain.CurrentDomain.GetAssemblies();
			for (var i = 0; i < assemblies.Length; i++)
			{
				var types = assemblies[i].GetTypes();

				for (var j = 0; j < types.Length; j++)
				{
					var type = types[j];
					if (!type.IsDefined(typeof(InstallAttribute), false) ||
					    type.IsDefined(typeof(IgnoreAttribute), false))
						continue;

					var attribute = type.GetCustomAttribute(typeof(InstallAttribute), false) as InstallAttribute;

					foreach (var installerTemplate in EcsInstallers)
					{
						if (!EnumUtils.HasFlag(attribute.Type, installerTemplate.TypeFlag))
							continue;

						AddToTemplate(installerTemplate, type, attribute);
					}
				}
			}

			foreach (var template in EcsInstallers)
			{
				var generatedInstallerCode =
					GenerateInstaller(template.Name, template.Container, template.Namespaces);
				template.GeneratedInstallerCode = generatedInstallerCode;
				Debug.Log($"[EcsInstallerGenerator] Generated {template.Type}: {template.Counter}");
			}

			var generatedFiles = EcsInstallers
				.Select(s => new GeneratedFile(s.Name, s.GeneratedInstallerCode))
				.ToList();
			EcsInstallers.Clear();
			generatedFiles.Add(ExecutionTypesGenerator.Generate());
			return generatedFiles.ToArray();
		}

		private static string GenerateInstaller(
			string name,
			Dictionary<ExecutionPriority, List<TypeInfo>> container,
			HashSet<string> nameSpaces
		)
		{
			var ns = EcsInstallerGeneratorTemplates.GetNamespaces(nameSpaces, container);
			var methods = container.Keys.Select(s => s.ToString())
				.Select(EcsInstallerGeneratorTemplates.GetMethod)
				.Join("\n");
			var body = container
				.Select(s => EcsInstallerGeneratorTemplates.GetMethodBody(
					s.Key.ToString(),
					EcsInstallerGeneratorTemplates.GetBinds(s.Value)
				))
				.Join("\n\n");

			return EcsInstallerGeneratorTemplates.GetInstaller(name, ns, methods, body);
		}

		private static void AddToTemplate(
			EcsInstallerTemplate template,
			Type type,
			InstallAttribute attribute
		)
		{
			template.Counter++;
			template.Container[attribute.Priority].Add(new TypeInfo(type, attribute));
			template.Namespaces.Add(type.Namespace);
		}
	}
}