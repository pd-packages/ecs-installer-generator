# Changelog

---

## [v1.0.3](https://gitlab.com/pd-packages/ecs-installer-generator/-/tags/v1.0.3)

### Added

- Get namespaces from BindOverride

---

## [v1.0.2](https://gitlab.com/pd-packages/ecs-installer-generator/-/tags/v1.0.2)

### Added

- Add Struct to AttributeUsage to Install, BindOverride, Ignore, BindRule attributes

---

## [v1.0.1](https://gitlab.com/pd-packages/ecs-installer-generator/-/tags/v1.0.1)

### Fixed

- Namespace in generated scripts

---

## [v1.0.0](https://gitlab.com/pd-packages/ecs-installer-generator/-/tags/v1.0.0)

### Added

- Project beginning

---

## [v0.0.0](https://gitlab.com/pd-packages/ecs-installer-generator/-/tags/v0.0.0)

### Added

- For new features.

### Changed

- For changes in existing functionality.

### Deprecated

- For soon-to-be removed features.

### Removed

- For now removed features.

### Fixed

- For any bug fixes.

### Security

- In case of vulnerabilities.
