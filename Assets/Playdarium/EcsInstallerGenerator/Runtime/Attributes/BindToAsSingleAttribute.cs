using System;
using Playdarium.EcsInstallerGenerator.Runtime.Enums;

namespace Playdarium.EcsInstallerGenerator.Runtime.Attributes
{
	public sealed class BindToAsSingleAttribute : BindOverrideAttribute
	{
		private readonly Type _contractType;
		private readonly EBindScope _scope;

		public BindToAsSingleAttribute(Type contractType, EBindScope scope = EBindScope.Single)
		{
			_contractType = contractType;
			_scope = scope;
		}

		public override string Create(string containerName, Type type, int order, string features)
			=> $"{containerName}.Bind<{_contractType.FullName}>().To<{type.Name}>().As{_scope}(); // {order:0000} {features}";
	}
}