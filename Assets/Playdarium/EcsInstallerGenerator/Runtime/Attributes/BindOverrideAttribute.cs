using System;
using System.Collections.Generic;

namespace Playdarium.EcsInstallerGenerator.Runtime.Attributes
{
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct)]
	public abstract class BindOverrideAttribute : Attribute
	{
		public abstract string Create(
			string containerName,
			Type type,
			int order,
			string features
		);

		public virtual List<string> GetNamespaces(Type type) => new() { type.Namespace };
	}
}