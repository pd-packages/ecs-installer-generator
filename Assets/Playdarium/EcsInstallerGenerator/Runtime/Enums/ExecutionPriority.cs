﻿namespace Playdarium.EcsInstallerGenerator.Runtime.Enums {
	public enum ExecutionPriority {
		Urgent,
		High,
		Normal,
		Low,
		None
	}
}
