namespace Playdarium.EcsInstallerGenerator.Runtime.Impls
{
	public class AlwaysTrueBindRuleMatcher : IBindRuleMatcher
	{
		public bool Match<T>() where T : IDefiniteBindRuled => true;
	}
}