﻿using System;
using Playdarium.EcsInstallerGenerator.Runtime.Enums;

namespace Playdarium.EcsInstallerGenerator.Runtime.Attributes
{
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct)]
	public class InstallAttribute : Attribute
	{
		private static readonly string[] EmptyFeature = { "" };

		public readonly long Type;
		public readonly ExecutionPriority Priority;
		public readonly int Order;
		public readonly string[] Features;

		public InstallAttribute(
			long type,
			params string[] features
		) : this(type, ExecutionPriority.None, 0, features)
		{
		}

		public InstallAttribute(
			long type,
			ExecutionPriority priority,
			params string[] features
		) : this(type, priority, 0, features)
		{
		}

		public InstallAttribute(
			long type,
			ExecutionPriority priority,
			int order,
			params string[] features
		)
		{
			Type = type;
			Priority = priority;
			Order = order;
			Features = features.Length != 0 ? features : EmptyFeature;
		}
	}
}