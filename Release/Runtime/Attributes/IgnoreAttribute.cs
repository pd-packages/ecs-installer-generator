﻿using System;

namespace Playdarium.EcsInstallerGenerator.Runtime.Attributes
{
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct)]
	public class IgnoreAttribute : Attribute
	{
	}
}