using System;

namespace Playdarium.EcsInstallerGenerator.Runtime.Attributes
{
	[AttributeUsage(AttributeTargets.Class)]
	public class DebugSystemAttribute : Attribute
	{
	}
}