namespace Playdarium.EcsInstallerGenerator.Runtime.Enums
{
	public enum EBindScope : byte
	{
		Single,
		Transient,
		Cached
	}
}