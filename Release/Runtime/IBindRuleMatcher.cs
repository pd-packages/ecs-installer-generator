namespace Playdarium.EcsInstallerGenerator.Runtime
{
	public interface IBindRuleMatcher
	{
		bool Match<T>() where T : IDefiniteBindRuled;
	}
}