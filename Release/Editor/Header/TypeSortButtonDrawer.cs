using Playdarium.EcsInstallerGenerator.Sort.Impls;
using UnityEngine;

namespace Playdarium.EcsInstallerGenerator.Header
{
	internal class TypeSortButtonDrawer : ASortButtonDrawer
	{
		private readonly TypeSortStrategy _sortStrategy;

		public TypeSortButtonDrawer(TypeSortStrategy sortStrategy, GUILayoutOption[] style)
			: base(sortStrategy, style)
		{
			_sortStrategy = sortStrategy;
		}

		protected override string GetButtonLabel() => string.IsNullOrEmpty(_sortStrategy.Name)
			? "Type"
			: _sortStrategy.Name;
	}
}