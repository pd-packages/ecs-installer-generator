﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Playdarium.EcsInstallerGenerator.Header;
using Playdarium.EcsInstallerGenerator.Models;
using Playdarium.EcsInstallerGenerator.Runtime.Attributes;
using Playdarium.EcsInstallerGenerator.Runtime.Enums;
using Playdarium.EcsInstallerGenerator.Sort;
using Playdarium.EcsInstallerGenerator.Sort.Impls;
using Playdarium.EcsInstallerGenerator.Utils;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Playdarium.EcsInstallerGenerator.Windows
{
	internal partial class EcsInstallerGeneratorWindow : EditorWindow
	{
		private List<ASortButtonDrawer> _sortButtonDrawers;

		private readonly List<ISortStrategy> _sortStrategies = new();
		private readonly List<AttributeRecord> _attributes = new();
		private readonly List<AttributeRecord> _current = new();

		private string _savePath;
		private string _searchPath;
		private ReorderableList _executionTypesList;

		private Vector2 _scroll;
		private int _filterPriority;
		private int _filterType;
		private int _sortOrder;
		private ESortType _sorting;
		private int _counter;
		private string _search;
		private bool _options;

		private static bool _inProgress;

		[MenuItem("Tools/Ecs/Installer Generator Properties")]
		public static void Open()
		{
			var window = GetWindow<EcsInstallerGeneratorWindow>("Installer Generator");
			window.Show();
		}

		private void OnEnable()
		{
			wantsMouseEnterLeaveWindow = true;
			_options = false;
			_savePath = EcsInstallerSettings.SavePath;
			_searchPath = EcsInstallerSettings.SearchPath;

			var assemblies = AppDomain.CurrentDomain.GetAssemblies();
			for (var i = 0; i < assemblies.Length; i++)
			{
				var types = assemblies[i].GetTypes();
				for (var j = 0; j < types.Length; j++)
				{
					var type = types[j];
					if (!type.IsDefined(typeof(InstallAttribute), false))
						continue;
					var attribute = type.GetCustomAttribute<InstallAttribute>(false);
					_attributes.Add(new AttributeRecord(type, attribute));
				}
			}

			InitializeSortButtons();

			foreach (var buttonDrawer in _sortButtonDrawers)
			{
				_sortStrategies.Add(buttonDrawer.SortStrategy);
			}
		}

		private void InitializeSortButtons()
		{
			var prioritySort = new PrioritySortStrategy();
			var orderSort = new OrderSortStrategy();
			var typeSort = new TypeSortStrategy();
			var nameSort = new NameSortStrategy();
			var featureSort = new FeatureSortStrategy();

			var prioritySortDrawer = new PrioritySortButtonDrawer(prioritySort, ButtonsStyle);
			var orderSortDrawer = new OrderSortButtonDrawer(orderSort, ButtonsStyle);
			var typeSortDrawer = new TypeSortButtonDrawer(typeSort, TypeStyle);
			var nameSortDrawer = new NameSortButtonDrawer(nameSort, NameStyle);
			var featureSortDrawer = new FeatureSortButtonDrawer(featureSort, FeatureStyle, _attributes);

			prioritySortDrawer.ResetOnClick(orderSort, typeSort, nameSort);
			orderSortDrawer.ResetOnClick(prioritySort, typeSort, nameSort);
			typeSortDrawer.ResetOnClick(prioritySort, orderSort, nameSort);
			nameSortDrawer.ResetOnClick(prioritySort, orderSort, typeSort);

			_sortButtonDrawers = new List<ASortButtonDrawer>
			{
				prioritySortDrawer,
				orderSortDrawer,
				typeSortDrawer,
				nameSortDrawer,
				featureSortDrawer,
			};

			var list = new List<string>(EcsInstallerSettings.ExecutionTypes);
			_executionTypesList = new ReorderableList(list, typeof(string))
			{
				onChangedCallback = _ => EcsInstallerSettings.ExecutionTypes = list.ToArray(),
				drawElementCallback = (rect, index, active, focused) =>
				{
					using var change = new EditorGUI.ChangeCheckScope();
					list[index] = GUI.TextField(rect, list[index]);
					if (change.changed)
						EcsInstallerSettings.ExecutionTypes = list.ToArray();
				}
			};
		}

		private void OnGUI()
		{
			Header();
			Options();
			Search();
			ListHeader();
			List();
		}

		private void Header()
		{
			GUILayout.Space(5);
			GUILayout.BeginHorizontal();
			if (GUILayout.Button("Options", EditorStyles.toolbarButton, ButtonsStyle))
				_options = !_options;
			GUILayout.Label("", EditorStyles.toolbarButton, GUILayout.ExpandWidth(true));
			if (GUILayout.Button("Generate", EditorStyles.toolbarButton, ButtonsStyle))
				Generate();
			GUILayout.EndHorizontal();
			GUILayout.Space(5);
		}

		private void Options()
		{
			if (!_options)
				return;

			GUILayout.Label("Options", EditorStyles.boldLabel);

			EditorGUI.BeginChangeCheck();

			GUILayout.Label("Paths", EditorStyles.boldLabel);
			_savePath = EditorGUILayout.TextField("Installers", _savePath);
			_searchPath = EditorGUILayout.TextField("Search scripts", _searchPath);

			GUILayout.Space(5);
			GUILayout.Label("Paths", EditorStyles.boldLabel);

			_executionTypesList.DoLayoutList();

			if (EditorGUI.EndChangeCheck())
			{
				EcsInstallerSettings.SavePath = _savePath;
				EcsInstallerSettings.SearchPath = _searchPath;
			}

			GUILayout.Space(10);
		}

		private void Search()
		{
			using (new GUILayout.HorizontalScope())
			{
				GUILayout.Label("Search:", EditorStyles.toolbarButton, ButtonsStyle);
				EditorGUI.BeginChangeCheck();
				_search = GUILayout.TextField(_search, EditorStyles.toolbarTextField, GUILayout.ExpandWidth(true));
				if (EditorGUI.EndChangeCheck())
					ProcessSearch();
				GUILayout.Label($"{_current.Count}/{_attributes.Count}", EditorStyles.toolbarButton, ButtonsStyle);
				if (GUILayout.Button("Reset Filters", EditorStyles.toolbarButton, ButtonsStyle))
				{
					foreach (var buttonDrawer in _sortButtonDrawers)
					{
						var sortStrategy = buttonDrawer.SortStrategy;
						sortStrategy.Reset();
					}
				}
			}
		}


		private void ListHeader()
		{
			GUILayout.BeginHorizontal();

			ProcessSearch();
			ProcessSortButtonClick();
			ProcessSort();

			GUILayout.Space(20f);
			GUILayout.EndHorizontal();
		}

		private void ProcessSortButtonClick()
		{
			for (var i = 0; i < _sortButtonDrawers.Count; i++)
			{
				var buttonDrawer = _sortButtonDrawers[i];
				var sortStrategy = buttonDrawer.SortStrategy;
				if (!buttonDrawer.OnClick())
					continue;

				sortStrategy.Next();
				_sortStrategies.Remove(sortStrategy);
				_sortStrategies.Insert(_sortStrategies.Count, sortStrategy);
				break;
			}
		}

		private void ProcessSort()
		{
			foreach (var sortStrategy in _sortStrategies)
				sortStrategy.Sort(_current);
		}

		private void List()
		{
			_scroll = GUILayout.BeginScrollView(_scroll, false, true);
			foreach (var attribute in _current)
				Row(attribute);
			GUILayout.EndScrollView();
		}

		private void Row(AttributeRecord record)
		{
			GUILayout.BeginHorizontal();
			EditorGUI.BeginChangeCheck();

			record.Changes.Priority = (ExecutionPriority)Enum(record.Changes.Priority);
			record.Changes.Order =
				EditorGUILayout.IntField(record.Changes.Order, EditorStyles.miniTextField, ButtonsStyle);
			var executionTypes = EnumUtils.ExecutionTypeMaskToValuesArray(record.Changes.Type);
			EditorGUILayout.LabelField(string.Join(", ", executionTypes), TypeStyle);
			if (GUILayout.Button(record.Type.Name, EditorStyles.miniButton, NameStyle))
			{
				GUI.changed = false;
				var selection = AssetDatabase.LoadAssetAtPath(FindFileForSelection(record.Type.Name), typeof(Object));
				Selection.activeObject = selection;
				AssetDatabase.OpenAsset(selection);
			}

			GUILayout.Label(string.Join(", ", record.Features), EditorStyles.miniLabel, FeatureStyle);

			GUILayout.EndHorizontal();
		}

		private void ProcessSearch()
		{
			_current.Clear();
			_current.AddRange(_attributes);

			if (string.IsNullOrEmpty(_search))
				return;

			var search = _search.ToLower();
			_current.RemoveAll(x => !x.Type.Name.ToLower().Contains(search));
		}

		[MenuItem("Tools/Ecs/Generate Installers &g")]
		public static void Generate()
		{
			if (_inProgress)
				return;

			EditorUtility.DisplayProgressBar("Ecs Installer", "Generate...", .1f);

			try
			{
				_inProgress = true;
				var generatedFiles = Generators.EcsInstallerGenerator.Generate();
				var path = EcsInstallerSettings.SavePath;

				foreach (var template in generatedFiles)
				{
					SaveToFile(template.GeneratedInstallerCode, $"{template.Name}.cs", path);
				}
			}
			finally
			{
				_inProgress = false;
				EditorUtility.ClearProgressBar();
			}

			AssetDatabase.Refresh();
		}
	}
}