using System.Collections.Generic;
using Playdarium.EcsInstallerGenerator.Models;

namespace Playdarium.EcsInstallerGenerator.Sort.Impls
{
	internal class TypeSortStrategy : ISortStrategy
	{
		private static string[] Types => EcsInstallerSettings.ExecutionTypes;

		private int _filterType;

		public string Name => !IsFilterEmpty() ? Types[_filterType - 1] : string.Empty;

		public void Reset() => _filterType = 0;

		public void Next() => _filterType = (_filterType + 1) % Types.Length;

		public void Sort(List<AttributeRecord> records)
		{
			if (IsFilterEmpty())
				return;

			var filterPriority = 1 << _filterType;
			records.Sort((record1, record2) =>
			{
				var type1 = record1.Attribute.Type;
				var type2 = record2.Attribute.Type;

				if (type1 == filterPriority && type2 != filterPriority)
					return 1;
				if (type1 != filterPriority && type2 == filterPriority)
					return -1;
				return 0;
			});
		}

		private bool IsFilterEmpty() => _filterType == 0;
	}
}