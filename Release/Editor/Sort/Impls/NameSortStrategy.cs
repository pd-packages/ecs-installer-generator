using Playdarium.EcsInstallerGenerator.Models;

namespace Playdarium.EcsInstallerGenerator.Sort.Impls
{
	internal class NameSortStrategy : AStringSortStrategy
	{
		protected override string GetValue(AttributeRecord record) => record.Type.Name;
	}
}