using System.Linq;
using Playdarium.EcsInstallerGenerator.Models;

namespace Playdarium.EcsInstallerGenerator.Generators
{
	internal static class ExecutionTypesGenerator
	{
		public const string CLASS_NAME = "ExecutionType";

		public static GeneratedFile Generate()
		{
			var fileContent = GenerateFileContent();
			return new GeneratedFile(CLASS_NAME, fileContent);
		}

		private static string GenerateFileContent()
		{
			var executionTypes = EcsInstallerSettings.ExecutionTypes;
			var number = 1;
			var executionTypeValues = executionTypes
				.Select(s =>
				{
					var enumValue = CreateEnumValue(s, number);
					number += number;
					return enumValue;
				})
				.ToArray();
			return CreateEnum(executionTypeValues);
		}

		private static string CreateEnumValue(string name, int value)
			=> $@"		public const long {name} = {value};";

		private static string CreateEnum(string[] values) =>
			$@"{EcsInstallerConstants.GeneratedComment}
namespace {EcsInstallerConstants.NAMESPACE} 
{{
	public struct {CLASS_NAME}
	{{
{string.Join("\n", values)}
	}}
}}";
	}
}