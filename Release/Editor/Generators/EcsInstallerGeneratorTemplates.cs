using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Playdarium.EcsInstallerGenerator.Runtime;
using Playdarium.EcsInstallerGenerator.Runtime.Attributes;
using Playdarium.EcsInstallerGenerator.Runtime.Enums;
using Playdarium.EcsInstallerGenerator.Runtime.Impls;
using Playdarium.EcsInstallerGenerator.Utils;
using TypeInfo = Playdarium.EcsInstallerGenerator.Models.TypeInfo;

namespace Playdarium.EcsInstallerGenerator.Generators
{
	public static class EcsInstallerGeneratorTemplates
	{
		public static string GetNamespaces(HashSet<string> namespaces, Dictionary<ExecutionPriority, List<TypeInfo>> container)
		{
			foreach (var typeInfos in container.Values)
			{
				foreach (var typeInfo in typeInfos)
				{
					var bindOverride = typeInfo.Type.GetCustomAttribute<BindOverrideAttribute>();
					if (bindOverride == null)
						continue;

					var list = bindOverride.GetNamespaces(typeInfo.Type);
					namespaces.UnionWith(list);
				}
			}

			return namespaces.Select(s => $"using {s};").Join("\n");
		}

		public static string GetBinds(List<TypeInfo> types)
			=> types.OrderBy(s => s.Order)
				.Select(GetBind)
				.Join("\n");

		private static string GetBind(TypeInfo typeInfo)
			=> HasBindRule(typeInfo, out var rule)
				? GetBindWithRule(rule, typeInfo)
				: $"			{GetBindMethod(typeInfo)}";

		private static string GetBindWithRule(string rule, TypeInfo typeInfo)
			=> $@"			if({rule})
			{{
				{GetBindMethod(typeInfo)}
			}}";

		private static bool HasBindRule(TypeInfo typeInfo, out string rule)
		{
			var rules = new List<string>();

			if (typeInfo.HasAttribute<DebugSystemAttribute>())
				rules.Add("isDebug");

			if (typeInfo.Type.IsDefined(typeof(BindRuleAttribute)))
				rules.Add($"rule.Match<{typeInfo.Type.Name}>()");

			rule = string.Join(" && ", rules);
			return rules.Count > 0;
		}

		private static string GetBindMethod(TypeInfo typeInfo)
		{
			var bindOverride = typeInfo.Type.GetCustomAttribute<BindOverrideAttribute>();
			return bindOverride?.Create("container", typeInfo.Type, typeInfo.Order, typeInfo.Features)
			       ?? $"container.BindInterfacesTo<{typeInfo.Type.Name}>().AsSingle();  // {typeInfo.Order:0000} {typeInfo.Features}";
		}

		public static string GetMethod(string name)
			=> $"			{name}(container, isDebug, rule);";

		public static string GetMethodBody(string name, string body)
			=> $@"		private static void {name}(DiContainer container, bool isDebug, {nameof(IBindRuleMatcher)} rule)
		{{
{body}
		}}";

		public static string GetInstaller(
			string name,
			string ns,
			string methods,
			string body
		)
			=> $@"{ns}
using Playdarium.EcsInstallerGenerator.Runtime.Impls;
using Playdarium.EcsInstallerGenerator.Runtime;
using Zenject;

{EcsInstallerConstants.GeneratedComment}
namespace {EcsInstallerConstants.NAMESPACE} 
{{
	public static class {name} 
	{{

		public static void Install(DiContainer container, bool isDebug = true, {nameof(IBindRuleMatcher)} rule = null) 
		{{
			rule ??= new {nameof(AlwaysTrueBindRuleMatcher)}();
{methods}
		}}

{body}
	}}
}}";
	}
}